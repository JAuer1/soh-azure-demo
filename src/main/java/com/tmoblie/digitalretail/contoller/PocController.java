package com.tmoblie.digitalretail.contoller;

import com.tmoblie.digitalretail.service.PocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PocController {

    private final PocService pocService;

    @Autowired
    public PocController(PocService pocService){
        this.pocService = pocService;
    }

    @PutMapping(value = "/sendToAEH", produces = "application/json")
    public ResponseEntity<String> pocDefaultMessage() {
        return new ResponseEntity<>(pocService.pocSendDefaultAEHRequest(), HttpStatus.ACCEPTED);
    }

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

}
