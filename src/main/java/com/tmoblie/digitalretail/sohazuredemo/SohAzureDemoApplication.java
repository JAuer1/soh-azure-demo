package com.tmoblie.digitalretail.sohazuredemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.tmoblie.digitalretail"})
public class SohAzureDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SohAzureDemoApplication.class, args);
	}

}
