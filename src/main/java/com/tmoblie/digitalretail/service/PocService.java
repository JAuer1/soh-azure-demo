package com.tmoblie.digitalretail.service;

import com.azure.messaging.eventhubs.EventData;
import com.azure.messaging.eventhubs.EventDataBatch;
import com.azure.messaging.eventhubs.EventHubClientBuilder;
import com.azure.messaging.eventhubs.EventHubProducerClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoblie.digitalretail.dto.ItemInfo;
import com.tmoblie.digitalretail.dto.ProductDetails;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class PocService {

    private static final String CONNECTION_STRING = "THIS IS A DUMMY CONNECTION STRING";
    private static final String EVENT_HUB_NAME = "soh-poc-hub";
    private final ObjectMapper objectMapper = new ObjectMapper();

    public String pocSendDefaultAEHRequest() {
        try {
            sendToEventHub(buildProductDetailToSend());
            return "Messages sent as expected";
        } catch(JsonProcessingException jpe) {
            return jpe.toString();
        }
    }

    private ProductDetails buildProductDetailToSend() {
        return ProductDetails.builder()
                .deliveryMethod("hand")
                .destinationPoint("7881")
                .storeToDCHandDeliverItems(Collections.emptyList())
                .storeToStoreHandDeliveryItems(buildItems())
                .storeToStoreUpsDeliveryItems(Collections.emptyList())
                .storeToDCUpsDeliveryItems(Collections.emptyList())
                .build();
    }

    private List<ItemInfo> buildItems() {
        return Collections.singletonList(ItemInfo.builder()
                .boxNum("1")
                .quantity(1)
                .returnCenter("55")
                .serialNumber("359221106947889")
                .serialNumberManualEntryFlag(true)
                .sku("6347785")
                .skuDescription("Google Pixel 3a 64GB")
                .skuManualEntryFlag(true)
                .build());
    }

    private void sendToEventHub(ProductDetails productDetails) throws JsonProcessingException {
        try (EventHubProducerClient producer = new EventHubClientBuilder()
                .connectionString(CONNECTION_STRING, EVENT_HUB_NAME)
                .buildProducerClient()) {
            EventDataBatch batch = producer.createBatch();
            batch.tryAdd(new EventData(objectMapper.writeValueAsString(productDetails)));
            producer.send(batch);
        }
    }
}
