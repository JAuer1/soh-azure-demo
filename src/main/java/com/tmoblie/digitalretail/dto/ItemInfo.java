package com.tmoblie.digitalretail.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemInfo {
    private String boxNum;
    private Integer quantity;
    private String returnCenter;
    private String serialNumber;
    private Boolean serialNumberManualEntryFlag;
    private String sku;
    private String skuDescription;
    private Boolean skuManualEntryFlag;
}
