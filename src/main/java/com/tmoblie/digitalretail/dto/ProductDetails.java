package com.tmoblie.digitalretail.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDetails {
    private String deliveryMethod;
    private String destinationPoint;
    private List<ItemInfo> storeToDCHandDeliverItems;
    private List<ItemInfo> storeToDCUpsDeliveryItems;
    private List<ItemInfo> storeToStoreHandDeliveryItems;
    private List<ItemInfo> storeToStoreUpsDeliveryItems;
}
